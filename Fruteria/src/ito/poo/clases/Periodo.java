package ito.poo.clases;
import java.util.ArrayList;

import ito.poo.clases.Fruta;

public class Periodo {

	private ArrayList<String> tiempoCosecha;
	private ArrayList<Float> cantCosechaxTiempo;
	
	public Periodo(String timpo, float cantidad){
		super();
		this.tiempoCosecha = new ArrayList<String>();
		this.cantCosechaxTiempo = new ArrayList<Float>();
		
		this.tiempoCosecha.add(timpo);
		this.cantCosechaxTiempo.add(cantidad);
		
	}
	
	public float costoPeriodo(int i, float cantidad) {
		float l= 0f;
		if(this.cantCosechaxTiempo.size()>i) {
			l=this.cantCosechaxTiempo.get(i);
			l=l*cantidad;
		}
		
		return l;
	}
	
	public float gananciaEstimada(int i, float cantidad) {
		
		float l= 0f;
		if(this.cantCosechaxTiempo.size()>i) {
			l=this.cantCosechaxTiempo.get(i);
			l=l*cantidad;
		}
		
		return l;
	}
	
	@Override
	public String toString() {
		return "Periodo [tiempoCoshecha=" + tiempoCosecha + ", cantCosechaxTiempo=" + cantCosechaxTiempo + "]";
	}
	
	
	
}
