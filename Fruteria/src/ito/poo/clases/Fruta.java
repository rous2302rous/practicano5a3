package ito.poo.clases;
import ito.poo.clases.Periodo;
import java.util.ArrayList;

public class Fruta {

	private String nombre;
	private float extencion;
	private float costoPromedio;
	private float precioVentaProm;
	private ArrayList<Periodo> Periodo;
	
	public Fruta(String nombre, float extencion, float costoPromedio, float precioVentaProm) {
		super();
		this.nombre = nombre;
		this.extencion = extencion;
		this.costoPromedio = costoPromedio;
		this.precioVentaProm = precioVentaProm;
		this.Periodo = new ArrayList<Periodo>();
	}
	
	public void AgregarPeriodo(Periodo p) {
		
		Periodo.add(p);
		
	}
	
	public boolean EliminarPeriodo(int i) {
		boolean l = false;
		if(this.Periodo.size()>i) {
			this.Periodo.remove(i);
			l=true;
		}
		return l;
	}
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public float getExtencion() {
		return extencion;
	}
	public void setExtencion(float extencion) {
		this.extencion = extencion;
	}
	public float getCostoPromedio() {
		return costoPromedio;
	}
	public void setCostoPromedio(float costoPromedio) {
		this.costoPromedio = costoPromedio;
	}
	public float getPrecioVentaProm() {
		return precioVentaProm;
	}
	public void setPrecioVentaProm(float precioVentaProm) {
		this.precioVentaProm = precioVentaProm;
	}
	@Override
	public String toString() {
		return "Periodo [nombre=" + nombre + ", extencion=" + extencion + ", costoPromedio=" + costoPromedio
				+ ", precioVentaProm=" + precioVentaProm + ", Periodo=" + Periodo + "]";
	}
	
}
